'use strict';

var gulp 			    = require('gulp');
var sass        	= require('gulp-sass');
// var compass       = require('gulp-compass');
var imageResize   = require('gulp-image-resize');
var pug           = require('gulp-pug');
var sourcemaps 		= require('gulp-sourcemaps');
var autoprefixer 	= require('gulp-autoprefixer');
var browserSync 	= require('browser-sync').create();
var reload      	= browserSync.reload;
var imgDest       = './public/img'

// Static server
// gulp.task('server', ['sass', 'jade', 'copy_images', 'copy_icons'], function() {
gulp.task('server', ['sass', 'pug', 'copy_images', 'copy_fonts', 'copy_js', 'resize'], function() {
	browserSync.init({
		reloadDelay: 500,
		notify: false,
		open: false,
		server: {
			baseDir: "./public"
		},
		ghostMode: false
	});

  gulp.watch("sass/**/*.scss", ['sass']);
  gulp.watch("img/*", ['copy_images', 'resize']);
  gulp.watch("img", ['copy_images', 'resize']);
  gulp.watch("fonts", ['copy_fonts']);
  gulp.watch("pug/**/*.pug", ['pug']);
  gulp.watch("public/*.html", reload);
  gulp.watch("./js/**/*.js", ['copy_js']);
});

gulp.task('resize', function () {
  var images = {
    '1':'game-audio.jpg',
    '2':'game-bomb.jpg',
    '3':'game-cyberpong.jpg',
    '1':'game-fancy-skiing.jpg',
    '2':'game-fruit.jpg',
    '3':'game-funhouse.jpg',
    '1':'game-holopoint.jpg',
    '2':'game-jobsimulator.jpg',
    '3':'game-lightblade.jpg',
    '1':'game-old-fiend.jpg',
    '2':'game-raw-data.jpg',
    '3':'game-sefietennis.jpg',
    '1':'game-serious-sam.jpg',
    '2':'game-space-pirate.jpg',
    '3':'game-the-brookhaven.jpg',
    '1':'game-the-lab.jpg',
    '2':'game-theblu.jpg',
    '3':'game-unbreakable.jpg',
    '1':'game-vaniszing.jpg',
    '2':'game-waltz.jpg'
  };
  gulp.src([
      'img/game-space-pirate.jpg',
      'img/game-theblu.jpg',
      'img/game-audio.jpg',
      'img/game-fancy-skiing.jpg',
      'img/game-old-friend.jpg',
      'img/game-the-lab.jpg',
      'img/game-sefietennis.jpg',
      'img/game-vaniszing.jpg'
    ])
    .pipe(imageResize({
      width : 800,
      height : 500,
      crop : true,
      upscale : false
    }))
    .pipe(gulp.dest(imgDest));

  gulp.src([
      'img/game-holopoint.jpg',
      'img/game-bomb.jpg',
      'img/game-serious-sam.jpg',
      'img/game-jobsimulator.jpg',
      'img/game-raw-data.jpg',
      'img/game-waltz.jpg',
      'img/game-lightblade.jpg',
      'img/game-vanish.jpg'
    ])
    .pipe(imageResize({
      width : 600,
      height : 800,
      crop : true,
      upscale : false
    }))
    .pipe(gulp.dest(imgDest));

  gulp.src([
      'img/game-fruit.jpg',
      'img/game-cyberpong.jpg',
      'img/game-funhouse.jpg',
      'img/game-the-brookhaven.jpg',
      'img/game-unbreakable.jpg'
    ])
    .pipe(imageResize({
      width : 800,
      height : 800,
      crop : true,
      upscale : false
    }))
    .pipe(gulp.dest(imgDest));
});

gulp.task('sass', function () {
    return gulp.src('./sass/main.scss')
    	.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: '../sass'
        }))
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.stream());
});

gulp.task('pug', function() {
  return gulp.src('./pug/**/!(_)*.pug')
    .pipe(pug().on('error', sass.logError))
    .pipe(gulp.dest('./public'));
});

gulp.task('copy_images', function () {
  return gulp
    .src('img/*')
    .pipe(gulp.dest(imgDest));
});

gulp.task('copy_fonts', function () {
  return gulp
    .src('fonts/*')
    .pipe(gulp.dest('./public/fonts'));
});

gulp.task('copy_icons', function () {
  return gulp
    .src('icons/*')
    .pipe(gulp.dest('./public'));
});

gulp.task('copy_js', function () {
  return gulp
    .src([
          'js/jquery.min.js', 
          'js/parallax.min.js',
          'js/glitch.jquery.js',
          'js/mgGlitch.min.js',
          'js/main.js'])
    // .pipe(concat('all.js'))
    .pipe(gulp.dest('./public/js'));
});

// gulp.task('build', ['sass', 'jade', 'copy_images', 'copy_icons']);
gulp.task('build', ['sass', 'pug', 'copy_images', 'resize']);

gulp.task('default', ['server']);
