var map_style = [
  {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [{
          "visibility": "on"
      }, {
          "color": "#475a8b"
      }]
  }, {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [{
          "color": "#6782ba"
      }]
  }, {
      "featureType": "water",
      "elementType": "labels.text.stroke",
      "stylers": [{
          "visibility": "off"
      }]
  }, {
      "featureType": "landscape",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#3b4360"
      }]
  }, {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [{
          "visibility": "off"
      }]
  }, {
      "featureType": "landscape.man_made",
      "elementType": "geometry.fill",
      "stylers": [{
          "visibility": "on"
      }, {
          "color": "#3b4360"
      }]
  }, {
      "featureType": "landscape.man_made",
      "elementType": "geometry.stroke",
      "stylers": [{
          "color": "#475a8b"
      }]
  }, {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [{
          "visibility": "off"
      }]
  }, {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#6e89c0"
      }]
  }, {
      "featureType": "road",
      "elementType": "labels.text.stroke",
      "stylers": [{
          "visibility": "on"
      }, {
          "color": "#3b4360"
      }]
  }, {
      "featureType": "poi",
      "elementType": "labels.text.stroke",
      "stylers": [{
          "color": "#3b4360"
      }]
  }, {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [{
          "color": "#ccddfb"
      }]
  }, {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [{
          "color": "#ccddfb"
      }]
  }, {
      "elementType": "labels.text.stroke",
      "stylers": [{
          "color": "#3b4360"
      }]
  }, {
      "elementType": "labels.text.fill",
      "stylers": [{
          "color": "#b0c5eb"
      }]
  }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
          "color": "#2f3756"
      }]
  }, {
      "featureType": "transit",
      "elementType": "geometry.fill",
      "stylers": [{
          "color": "#2f3756"
      }]
  }, {
      "featureType": "road",
      "elementType": "geometry.stroke",
      "stylers": [{
          "visibility": "off"
      }]
}];
var map_options = {
    zoom: 15,
    scrollwheel: false,
    disableDefaultUI: true,
    zoomControl: true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.RIGHT_BOTTOM
    },
    center: { lat: 55.790362, lng: 37.530055 },
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: map_style
};
var pin_start = 'img/pin.svg';

var vr = {
  init: function(settings) {
      $('body').scrollspy({ target: '#navbar' })
      vr.showHideDescrSetup();
      vr.moreLessSetup();
      vr.youtubeModalSetup();
      vr.mapSetup(settings);
      vr.addrToggleSetup();
      vr.setupVideo();
      vr.mobileMenuSetup();
      vr.loadFromBottom();
      
      if (document.body.clientWidth > 1023) {
        vr.rotate3D();
        vr.glitchSetup();
        vr.glitch2Setup();
      }
  },
  showHideDescrSetup: function() {
      $('.js-show-descr').click(function() {
          $(this).closest('.thumbnail-vr').addClass('state-descr');
          return false;
      });

      $('.thumbnail-vr').click(function() {
          if ($(this).hasClass('state-descr')) {
              $(this).removeClass('state-descr');
          }
      });

      $('.js-hide-descr').click(function() {
          $(this).closest('.thumbnail-vr').removeClass('state-descr');
      });
  },
  moreLessSetup: function() {
      var $btnLess = $('.js-game-btn-less');
      var $btnMore = $('.js-game-btn-more');
      var $moreGames = $('.more-games');

      $btnMore.click(function() {
          $moreGames.css('max-height', '20000px');
          $btnLess.css('display', 'inline-block');
          $btnMore.hide();
      });

      $btnLess.click(function() {
          $moreGames.css('max-height', '0px');
          $btnMore.css('display', 'inline-block');
          $btnLess.hide();
      });
  },
  youtubeModalSetup: function() {
      var $youtubeModal = $('#youtubeModal');

      $youtubeModal.on('shown.bs.modal', function(e) {

          var youtubeCode = $(e.relatedTarget).data('youtube');

          $youtubeModal.find('.modal-body')
              .html('<iframe width="1280" ' +
                  'height="720" ' +
                  'src="https://www.youtube.com/embed/' + youtubeCode + '?rel=0&amp;showinfo=0" ' +
                  'frameborder="0" allowfullscreen></iframe>');
      });


      $youtubeModal.on('hide.bs.modal', function(e) {
          $youtubeModal.find('.modal-body').empty();
      });
  },
  glitchSetup: function() {
      $(".glitch-text").mgGlitch({
          // set 'true' to stop the plugin
          destroy: false,
          // set 'false' to stop glitching
          glitch: true,
          // set 'false' to stop scaling
          scale: true,
          // set 'false' to stop glitch blending
          blend: false,
          // select blend mode type
          blendModeType: 'hue',
          // set min time for glitch 1 elem
          glitch1TimeMin: 200,
          // set max time for glitch 1 elem
          glitch1TimeMax: 400,
          // set min time for glitch 2 elem
          glitch2TimeMin: 10,
          // set max time for glitch 2 elem
          glitch2TimeMax: 100,
      });
  },
  glitch2Setup: function() {
      $('.glitch2-text').glitch({
          maxint: 2,
          minint: 1,
          vshift: 1,
          hshift: 3
      });
      $('.glitch3-text').glitch({
          maxint: 3,
          minint: 1,
          vshift: 3,
          hshift: 3
      });
  },
  mapSetup: function(settings) {
      this.map = new google.maps.Map(document.getElementById("map"), settings.mapOptions);
      var marker_start = new google.maps.Marker({
          position: settings.mapOptions.center,
          map: this.map,
          title: 'VR Park',
          icon: settings.pinStart
      });
  },
  addrToggleSetup: function() {

      var $mapScreen = $('.map-screen'),
          $showMap = $('.js-show-map'),
          $listPoint = $('.list-point'),
          $hideMap = $('.js-hide-map');

      $showMap.click(function() {
          $hideMap.removeClass('active');
          $showMap.addClass('active');
          $mapScreen.addClass('map-shown');
          $listPoint.fadeOut();
      });

      $hideMap.click(function() {
          $hideMap.addClass('active');
          $showMap.removeClass('active');
          $mapScreen.removeClass('map-shown');
          $listPoint.fadeIn();
      });
  },
  setupVideo: function() {
      if ($('body').hasClass('video')) {
          var $h = $('#home');
          var videoSrc = $('body').data('video');
          if (document.body.clientWidth > 1023) {
              var w = $h.outerWidth();
              var $video = $('<div id="video_container" style="display:none">' +
                      '<video autoplay="autoplay" id="bgr_video" loop="loop"></video></div>')
                  .prependTo('body.video');

              var video = document.getElementById('bgr_video');
              video.addEventListener('loadeddata', function() {

                  $h.css('backgroud', 'none');

                  var jh = parseInt($h.outerHeight());
                  var videWidth = '100%';
                  var videHeight = 'auto'

                  if ($h.outerWidth() / $h.outerHeight() < 1920 / 1080) {
                      videWidth = 'auto';
                      videHeight = jh
                  }

                  $video
                      .css({
                          width: '100%',
                          height: jh,
                          overflow: 'hidden',
                          position: 'absolute',
                      })
                      .fadeIn(3000)
                      .find('video')
                      .css({
                          width: videWidth,
                          height: videHeight,
                          'min-width': '100%',
                          'min-height': '100%',
                          'position': 'absolute',
                          'top': '50%',
                          'left': '50%',
                          'transform': 'translate(-50%,-50%)',
                          display: 'block',
                      });
              }, false);

              video.src = videoSrc;
              video.load();
          }
      }
  },
  mobileMenuSetup: function(){

    var $navbar = $('#navbar');

    $navbar
      .on('show.bs.collapse', function () {
        $('body').addClass('modal-open');
    });

    $navbar.find('li a').click(function(){
        $('body').removeClass('modal-open');
        $navbar.collapse('hide');
    });

    $navbar.find('.close').click(function(){
      $navbar.collapse('hide');
      $('body').removeClass('modal-open');
    });
  },
  rotate3D: function(){
    $('.thumbnail-vr').css({
      '-webkit-transform-style': 'preserve-3d'
    });

    $('.descr, .game-descr').css({
      'position': 'absolute',
      '-webkit-transform': 'translateZ(20px)'
    });

    $('body').mousemove(function(event) {
        cx = Math.ceil($('body').width() / 2);
        cy = Math.ceil($('body').height() / 2);
        dx = event.clientX - cx;
        dy = event.clientY - cy;

        tiltx = (dy / cy);
        tilty = - (dx / cx);
        radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
        degree = (radius * 20);

        $('.thumbnail-vr')
          .css('transform','rotate3d(' + tiltx + ', ' + tilty + ', 0, ' + degree + 'deg)');
    });
  },
  loadFromBottom: function(){

    $elements = $('.site-wrapper, .map-screen, .fr-screen, .perspective');

    $elements.css({
      'opacity':'0',
      'transform': 'translateY(' + 6 + 'em)'
    });

    // Trigger fade in as window scrolls
    $(window).on('scroll load', function(){
      $elements.each( function(i){
        var bottom_of_object = $(this).offset().top + $(this).outerHeight()/8;
        var bottom_of_window = $(window).scrollTop() + $(window).height();
        if( bottom_of_window > bottom_of_object ){  
          $(this).css({'opacity':'1', 'transform': 'translateY('+0+'em)'});        
          $(this).find('.hidden-text').css({'opacity':'1'})
        } else {
          $(this).css({'opacity':'0', 'transform': 'translateY('+6+'em)'});
        }
      });
    });
  }
};

$(function() {
    vr.init({
        mapStyle: map_style,
        mapOptions: map_options,
        pinStart: pin_start
    });
});
